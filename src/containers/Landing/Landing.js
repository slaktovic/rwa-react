import React from 'react';
import landingImg from '../../assets/Images/pexels-photo-46274.jpeg';

import classes from './Landing.css';

const landing = (props) => (
    <div className={classes.Landing}>
        <img className={classes.LandingImg} src={landingImg}/>
    </div>
);

export default landing;