import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import Book from '../../components/Book/Book';
import Spinner from '../../components/UI/Spinner/Spinner';
import Aux from '../../hoc/Aux/Aux';

import classes from './Bestsellers.css';

class Bestsellers extends Component {

    componentDidMount() {
        this.props.fetchBooks();
        this.props.getFavoriteBooks();
    }

    addToFavoritesHandler = (book) => {
        this.props.onAddToFavorites(book);
    }

    render() {
        
        let books = <Spinner />
        let error = this.props.error 
            ? <h1 className={classes.Header}>{this.props.error}</h1> 
            : <h1 className={classes.Header}>Pick your favorite books!</h1>
        if(this.props.loading === false) {
            books = this.props.books.map(book => {
                return <li key={book.rank}>
                            <Book 
                                book={book} 
                                addToFavoritesHandler={this.addToFavoritesHandler}                          
                            />
                        </li>
            })
        }
        return (
            <Aux>
                {error}
                <ul className={classes.List}>
                    {books}
                </ul>   
            </Aux>         
        );
    }
}

const mapStateToProps = state => {
    return {
        books: state.books.books,
        loading: state.books.loading,
        favoriteBooks: state.favoritebooks.favoritebooks,
        error: state.favoritebooks.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchBooks: () => dispatch(actions.fetchBooks()),
        onAddToFavorites: (book) => dispatch(actions.addToFavorites(book)),
        getFavoriteBooks: () => dispatch(actions.getFavoriteBooks())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Bestsellers);