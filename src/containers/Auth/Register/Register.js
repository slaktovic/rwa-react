import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';

import classes from './Register.css';

class Register extends Component {
    state = {
        email: '',
        password: ''
    }

    handleEmailChange = (e) => {
        this.setState({email: e.target.value});
    };
     
    handlePasswordChange = (e) => {
        this.setState({password: e.target.value});
    }

    handleRegister = (event) => {
        event.preventDefault();
        const isLogin = false;
        this.props.onRegister(this.state.email,this.state.password, isLogin)
    }

    handleEnter  = (event) => {
        if(event.keyCode === 13){
            const isLogin = false;
            this.props.onRegister(this.state.email,this.state.password, isLogin)
        }
    }

    render() {
        let errorMessage = null;

        if ( this.props.error ) {
            errorMessage = (
                <p>{this.props.error.message}</p>
            );
        }

        return (
            <div>
                {errorMessage} 
                <form className={classes.Form}>
                    <input 
                        type="text" 
                        name="email" 
                        placeholder="Email" 
                        value={this.state.email} 
                        onChange={this.handleEmailChange} 
                        onKeyUp={this.handleEnter}/>
                    <input 
                        type="password" 
                        name="password" 
                        placeholder="Password" 
                        value={this.state.password} 
                        onChange={this.handlePasswordChange} 
                        onKeyUp={this.handleEnter}/>
                    <button className={classes.Button} type="button" onClick={this.handleRegister}>Register</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        error: state.auth.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onRegister: (email,password, isLogin) => dispatch(actions.auth(email,password,isLogin))
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(Register);