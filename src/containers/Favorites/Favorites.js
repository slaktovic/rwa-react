import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import Book from '../../components/Book/Book';
import Spinner from '../../components/UI/Spinner/Spinner';

import classes from './Favorites.css';

class Favorites extends Component {

    componentDidMount() {
        this.props.getFavoriteBooks();
    }

    render() {
        
        let books = <Spinner />

        if(this.props.loading === false) {
            books = this.props.favoriteBooks.map(book => {
                 return <li key={book.rank}>
                            <Book 
                                book={book} 
                                addToFavorites={false}
                            />
                        </li>
            })
        }
        return (
            
                <ul className={classes.List}>
                    {books}
                </ul>
              
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.favoritebooks.loading,
        favoriteBooks: state.favoritebooks.favoritebooks
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getFavoriteBooks: () => dispatch(actions.getFavoriteBooks()),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Favorites);