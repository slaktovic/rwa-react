import React from 'react';

import classes from './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = (props) => {
    if(props.isAuthenticated){
       return   <ul className={classes.NavigationItems}>
                    <NavigationItem link="/bestsellers">Bestsellers</NavigationItem>
                    <NavigationItem link="/favorites">Favorites</NavigationItem>
                    <NavigationItem link="/logout">Logout</NavigationItem>
                </ul>
    }else {
        return  <ul className={classes.NavigationItems}>
                    <NavigationItem link="/Login">Login</NavigationItem>
                    <NavigationItem link="/Register">Register</NavigationItem>
                </ul>
    }
}

export default navigationItems;