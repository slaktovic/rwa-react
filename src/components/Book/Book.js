import React from 'react';

import classes from './Book.css';

const book = (props) => (
    <div className={classes.Book}>
        <p className={classes.Paragraph}><strong>Rank:</strong> {props.book.rank}</p>
        <p className={classes.Paragraph}><strong>Title:</strong> {props.book.book_details[0].title}</p>
        <p className={classes.Paragraph}><strong>Author:</strong> {props.book.book_details[0].author}</p>
        <p className={classes.Paragraph}><strong>Description:</strong> {props.book.book_details[0].description}</p>
        <p className={classes.Paragraph}><strong>Publisher:</strong> {props.book.book_details[0].publisher}</p>
        {props.addToFavoritesHandler? <button className={classes.Button} onClick={ () => props.addToFavoritesHandler(props.book)}>Add to favorites</button> : <p></p>}               
    </div>
);
export default book;