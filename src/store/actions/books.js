import * as actionTypes from './actionTypes';

export const fetchBooks = () => {
    return {
        type: actionTypes.FETCH_BOOKS
    };
}

export const fetchBooksStart = () => {
    return {
        type: actionTypes.FETCH_BOOKS_START,
    };
}

export const fetchBooksSuccess = (books) => {
    return {
        type: actionTypes.FETCH_BOOKS_SUCCESS,
        books
    };
}

export const fetchBooksFail = (error) => {
    return {
        type: actionTypes.FETCH_BOOKS_FAIL,
        error
    };
}