import * as actionTypes from './actionTypes';

export const getFavoriteBooks = () => {
    return {
        type: actionTypes.GET_FAVORITE_BOOKS
    };
}

export const getFavoriteBooksStart = () => {
    return {
        type: actionTypes.GET_FAVORITE_BOOKS_START
    };
}

export const getFavoriteBooksSuccess = (books) => {
    return {
        type: actionTypes.GET_FAVORITE_BOOKS_SUCCESS,
        books
    };
}

export const getFavoriteBooksFail = (error) => {
    return {
        type: actionTypes.GET_FAVORITE_BOOKS_FAIL,
        error
    };
}

export const addToFavorites = (book) => {
    return {
        type: actionTypes.ADD_TO_FAVORITES,
        book
    };
}

export const addToFavoritesFail = (error) => {
    return {
        type: actionTypes.ADD_TO_FAVORITES_FAIL,
        error
    };
}