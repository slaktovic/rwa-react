import * as actionTypes from './actionTypes';

export const auth = (email,password, isLogin) => {
    return {
        type: actionTypes.AUTH_USER,
        email,
        password,
        isLogin
    }
}

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
}

export const authSuccess = (token,userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token,
        userId
    };
}

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error
    };
}

export const logout = () => {
    return {
        type: actionTypes.AUTH_INITIATE_LOGOUT
    };
}

export const logoutSucceed = () => {
    return {
      type: actionTypes.AUTH_LOGOUT
    };
};

export const authCheckState = () => {
    return {
      type: actionTypes.AUTH_CHECK_STATE
    };
};

export const checkAuthTimeout = expirationTime => {
    return {
      type: actionTypes.AUTH_CHECK_TIMEOUT,
      expirationTime: expirationTime
    };
};