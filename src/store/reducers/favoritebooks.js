import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    favoritebooks: [],
    error: null,
    loading: false
}

const addToFavorites = (state,action) => {
    return updateObject(state, {error: null, loading: false});
}

const addToFavoritesFail = (state,action) => {
    return updateObject(state, {error: action.error, loading: false});   
}

const getFavoriteBooksStart = (state,action) => {
    return updateObject(state, { error: null, loading: true});
}

const getFavoriteBooksSuccess = (state, action) => {
    return updateObject(state, { error: null, loading: false, favoritebooks: action.books})
}

const getFavoriteBooksFail = (state, action) => {
    return updateObject(state, { loading: false, error: action.error});
}

const reducer = (state = initialState,action) => {
    switch (action.type){
        case actionTypes.GET_FAVORITE_BOOKS_START: return getFavoriteBooksStart(state,action);
        case actionTypes.GET_FAVORITE_BOOKS_SUCCESS: return getFavoriteBooksSuccess(state,action);
        case actionTypes.GET_FAVORITE_BOOKS_FAIL: return getFavoriteBooksFail(state,action);
        case actionTypes.ADD_TO_FAVORITES: return addToFavorites(state,action);
        case actionTypes.ADD_TO_FAVORITES_FAIL: return addToFavoritesFail(state,action);
        default:
            return state;
    }
}

export default reducer;