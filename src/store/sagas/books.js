import { put} from 'redux-saga/effects';
import * as actions from "../actions/index";
import axios from 'axios';

export function* fetchBooksSaga(action){
    yield put(actions.fetchBooksStart());

    try {
        const response = yield axios.get('https://api.nytimes.com/svc/books/v3/lists.json?api-key=cfffd653482247979ff68c29e23eeadd&list=hardcover-fiction');
        yield put(actions.fetchBooksSuccess(response.data.results));
    }catch(error){
        yield put(actions.fetchBooksFail(error));
    }
}