import { put } from 'redux-saga/effects';
import * as actions from "../actions/index";
import axios from 'axios';

export function* getFavoriteBooksSaga(action){
    yield put(actions.getFavoriteBooksStart());

    try {
        const userId = localStorage.getItem('userId');
        const response = yield axios.get(`https://rwa-nyt-app.firebaseio.com/favoritebooks/${userId}.json`);
        let bookArr = [];
        for(let key in response.data){
            bookArr.push(response.data[key]);
        }
        yield put(actions.getFavoriteBooksSuccess(bookArr));
    }catch(error){
        yield put(actions.getFavoriteBooksFail());
    }
}

export function* addToFavoritesSaga(action) {
    try {
        const userId = localStorage.getItem('userId');

        const response = yield axios.get(`https://rwa-nyt-app.firebaseio.com/favoritebooks/${userId}.json`);
        let bookArr = [];
        for(let key in response.data){
            bookArr.push(response.data[key]);
        }
        let contained = false;
        for(let i = 0; i< bookArr.length; i++){
            if(bookArr[i].rank === action.book.rank){
                contained = true;
                break;
            }
        };
        if(contained) {
            yield put(actions.addToFavoritesFail('Book is already added to favorites!'));
        }else {
            yield axios.post(`https://rwa-nyt-app.firebaseio.com/favoritebooks/${userId}.json`, action.book);   
        }
    }catch(error){
        yield put(actions.addToFavoritesFail(error));
    }
}