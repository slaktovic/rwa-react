import { all, takeEvery } from "redux-saga/effects";
import * as actionTypes from "../actions/actionTypes";
import { authUserSaga, logoutSaga, authCheckStateSaga, checkAuthTimeoutSaga} from './auth';
import { fetchBooksSaga } from './books'; 
import { getFavoriteBooksSaga, addToFavoritesSaga } from "./favoritebooks";

export function* watchAuth() {
    yield all ([
        takeEvery(actionTypes.AUTH_USER, authUserSaga),
        takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga),
        takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga),
        takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga),
    ]);   
}

export function* watchBooks() {
    yield all ([
        takeEvery(actionTypes.FETCH_BOOKS, fetchBooksSaga)
    ]);
}

export function* watchFavoriteBooks() {
    yield all ([
        takeEvery(actionTypes.GET_FAVORITE_BOOKS, getFavoriteBooksSaga),
        takeEvery(actionTypes.ADD_TO_FAVORITES, addToFavoritesSaga)
    ]);
}