import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect} from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from './hoc/Layout/Layout';
import Register from './containers/Auth/Register/Register';
import Login from './containers/Auth/Login/Login';
import Logout from './containers/Auth/Logout/Logout';
import * as actions from './store/actions/index';
import Landing from './containers/Landing/Landing';
import Bestsellers from './containers/Bestsellers/Bestsellers';
import Favorites from './containers/Favorites/Favorites';

class App extends Component {
  componentDidMount () {
    this.props.onTryAutoSignup();
  }

  render() {

    let routes = (
      <Switch>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login} />
        <Route path="/" exact component={Landing} />
      </Switch>
    );

    if( this.props.isAuthenticated) {
      routes = (
        <Switch>
          <Route path="/bestsellers" component={Bestsellers} />
          <Route path="/favorites" component={Favorites} />
          <Route path="/logout" component={Logout} />
          <Redirect to="/bestsellers" />
        </Switch>
      );
    }

    return (
      <div >
          <Layout>
            {routes}
          </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch( actions.authCheckState() )
  };
};

export default withRouter( connect( mapStateToProps, mapDispatchToProps )( App ) );
